<?php
require_once "secure/config.php";
require_once "secure/functions.php";

$i = 1;
var_dump( $_POST );
var_dump( get_items_for_current_location() );

$cord = get_items_for_current_location();
if ( isset( $_POST['submit'] ) ) {
	create_checkpoint();
}
require_once "pages/header.php";
?>

<html>
<head>

    <style>
        #pointer_div {
            background-image: url('museum.jpg');
            width: 1250px;
            height: 90vh;
            margin: 0 auto;
        }

        #cross, .cross {
            position: relative;
            visibility: hidden;
            z-index: 2;
            width: 30px;
            margin: 0 0 0 -14px;
        }

        .form_pointer {

        }
    </style>


</head>
<body>

<form name="pointform" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?id_locations=1">

    <div class="form_pointer" id="pointer_div" onclick="point_it(event)">
        <img src="point.png" id="cross" style="">
    </div>
    <div class="row">
        <div class="col-lg-4">
            <input type="hidden" name="form_x">
            <input type="hidden" name="form_y">
            <div class="form-froup">
                <label for="title">Введите название объекта:</label>
                <input class="form-control" name="title" placeholder="Введите название объекта:" type="text">
            </div>
            <div class="form-froup">
                <label for="descript">Введите описание объекта:</label>
                <input class="form-control" name="descript" placeholder="Введите описание объекта:" type="text">
            </div>
            <div class="form-froup">
                <label for="author">Введите автора объекта:</label>
                <input class="form-control" name="author" placeholder="Введите автора объекта:" type="text">
            </div>
        </div>


    </div>
    <input class="btn btn-light" name="submit" type="submit">
</form>

<?php require_once "pages/footer.php"; ?>
<script>
    function point_it(event) {
        pos_x = event.offsetX ? (event.offsetX) : event.pageX - document.getElementById("pointer_div").offsetLeft;
        pos_y = event.offsetY ? (event.offsetY) : event.pageY - document.getElementById("pointer_div").offsetTop;

        console.log(pos_x);
        console.log(pos_y);

        document.getElementById("cross").style.left = (pos_x - 1);
        document.getElementById("cross").style.top = (pos_y - 15);
        document.getElementById("cross").style.visibility = "visible";
        document.pointform.form_x.value = pos_x;
        document.pointform.form_y.value = pos_y;
    }

    document.body.onload = addElement;

    var check_points = [


	    <?php foreach ($cord as $item): ?>
        [<?php echo $item['pos_x'] - $i * 16; ?>,
	        <?php echo $item['pos_y']; ?>],
	    <?php $i ++;?>
	    <?php endforeach; ?>


        // [420, 277],
        // [688 - 32, 331],
        // [421 - 32, 166],
        // [326 - 48, 464],
        // [326 - 64, 464],
        // [420 - 80, 277],
        // [420 - 96, 277],
        // [420 - 112, 277],
        // [420 - 128, 277],
    ];


    function addElement() {
        // create a new point elements
        for (var i = 0; i < check_points.length; i++) {
            var newPoint = document.createElement("img");
            newPoint.classList.add("cross");
            newPoint.setAttribute("src", "point.png");
            newPoint.style.visibility = "visible";

            newPoint.style.left = check_points[i][0] + "px";
            newPoint.style.top = check_points[i][1] + "px";

            // and give it some content
            var newContent = document.createTextNode("Hi there and greetings!");

            // add the newly created element and its content into the DOM
            var pointer_div = document.getElementById("pointer_div");

            // add the text node to the newly created div
            pointer_div.appendChild(newPoint);
        }
    }
</script>
</body>
</html>