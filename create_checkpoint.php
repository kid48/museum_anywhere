<?php
require_once "secure/config.php";
require_once "secure/functions.php";
$location = get_current_location();
$items = get_items_for_current_location();


?>
<!doctype html>
<html>
<head>
    <style>

        #hidden_block {
            display: none;
        }

        .save {
            background-color: #D7D8DF;
            width: 300px;
            border: solid 3px #0b2e13;
            padding: 12px 0;
            text-align: center;
            display: block;
        }

        .save:hover {
            background: #28a745;
        }

        h1 {
            margin: 0 !important;
            padding: 0;
            font-size: 27px !important;
        }

        header {
            width: auto;
            overflow: hidden;
        }

        #title, .save {
            float: left;
        }

        #title {
            background-color: #D7D8DF;
            width: 900px;
            border: solid 3px #0b2e13;
            padding: 8px 0;
        }

        #more {
            width: 150px;
        }

        #more:hover {
            cursor: pointer;
            background: #8da98b;
        }

        #visible {
            width: 150px;
        }

        #visible:hover {
            cursor: pointer;
            background: #8da98b;
        }

        .main {
            width: 1200px;
            margin: 0 auto;
            height: 90vh;
        }

        #wrapper {
            display: block;
            margin: 0 auto;
            width: 100%;
            overflow: hidden;
        }

        .side {
            background: #ada8d8;
            height: 100vh;
            width: 6%;
        }

        .side:hover {
            background: #8E9A92;
        }

        .side, .main {
            float: left;
        }

        #hidden_more1 {
            border: solid #000000 3px;
        }

        /*=====================================================*/

        #pointer_div {

            width: 1250px;
            height: 90vh;
            margin: 0 auto;
        }

        #cross, .cross {
            position: relative;
            visibility: hidden;
            z-index: 2;
            width: 20px;
            margin: 0 0 0 -14px;
        }

        /*=====================================================*/


    </style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="assets/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="assets/css/sb-admin.css" rel="stylesheet">

</head>
<body>


<div id="wrapper">
    <div class="side"></div>

    <div class="main">


        <header>

            <div id="title">
                <h1><?php echo $location['title']; ?> </h1>
            </div>

            <a id="next_link" href="#" class="save" onclick="if(next_link.getAttribute('href') === '#'){
                alert('Будь ласка, поставте точку')
            } " >
                next step
            </a>

            <section id="hidden_block">
                <p>
                    <?php echo $location['descript'] ?>
                </p>
            </section>

        </header>

        <form name="pointform" method="post">
            <div id="pointer_div" onclick="point_it(event)"
                 style=" background: url('<?php echo $location['img'] ?>') center 20%/100%;"></div>
        </form>

    </div>

    <div class="side"></div>
</div>


<script>
    var counter = 0;
    var next_link = document.querySelector("#next_link");



    function point_it(event) {
        var clickPoint = document.createElement("img");
        var pointer_div = document.getElementById("pointer_div");
        var all_points = document.querySelectorAll('.cross');

        var get_link = "",
            next_link = document.querySelector("#next_link");

        if (counter != 0) {
            all_points[all_points.length - 1].outerHTML = "";
        } else {
            counter++;
        }


        pos_x = event.offsetX ? (event.offsetX) : event.pageX - document.getElementById("pointer_div").offsetLeft;
        pos_y = event.offsetY ? (event.offsetY) : event.pageY - document.getElementById("pointer_div").offsetTop;

        clickPoint.classList.add("cross");
        clickPoint.setAttribute("src", "point.png");
        clickPoint.style.visibility = "visible";

        clickPoint.style.left = (pos_x) + "px";
        clickPoint.style.top = pos_y + "px";


        get_link = "?pos_x=" + pos_x + "&pos_y=" + pos_y + "&id=<?php echo $_GET['id'] ?>&id_objects=<?php echo $_GET['id_objects']  ?>";

        next_link_href = "http://localhost/museum_anywhere/create_new_checkpoint" + get_link;


        console.log(next_link_href);

        next_link.href = next_link_href;


        pointer_div.appendChild(clickPoint);

    }



    document.body.onload = function () {

        addElement();

        var all_points = document.querySelectorAll(".cross");

        var hidden_more = document.createElement('div');
        hidden_more.className = "alert alert-success";


        console.log(all_points);

        for (var a = 0; a < all_points.length; a++) {
            var KocTuJIb = [

                <?php foreach ($items as $item): ?>

                "<?php echo $item['descript'];?>",

                <?php endforeach; ?>

            ];

            all_points[a].addEventListener('click', function () {
                if (this.classList[1] == "cross_0") {
                    alert(KocTuJIb[0]);
                } else if (this.classList[1] == "cross_1") {
                    alert(KocTuJIb[1]);
                } else if (this.classList[1] == "cross_2") {
                    alert(KocTuJIb[2]);
                } else if (this.classList[1] == "cross_3") {
                    alert(KocTuJIb[3]);
                } else if (this.classList[1] == "cross_4") {
                    alert(KocTuJIb[4]);
                }

            });
        }
    };

    var check_points = [

        <?php foreach ($items as $item): ?>

        [<?php echo $item['pos_x'];?>,<?php echo $item['pos_y'];?>],

        <?php endforeach; ?>


//        [688 - 32, 331],
//        [326 - 48, 464],


        // [326 - 64, 464],
        // [420 - 80, 277],
        // [420 - 96, 277],
        // [420 - 112, 277],
        // [420 - 128, 277],
    ];


    function addElement() {


        // create a new point elements
        for (var i = 0; i < check_points.length; i++) {
            var newPoint = document.createElement("img");
            newPoint.classList.add("cross");
            newPoint.classList.add("cross_" + i);
            newPoint.setAttribute("src", "point.png");
            newPoint.style.visibility = "visible";

            newPoint.style.left = check_points[i][0] + "px";
            newPoint.style.top = check_points[i][1] + "px";

            // and give it some content
            var newContent = document.createTextNode("Hi there and greetings!");

            // add the newly created element and its content into the DOM
            var pointer_div = document.getElementById("pointer_div");

            // add the text node to the newly created div
            pointer_div.appendChild(newPoint);


        }
    }


</script>


<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<!--<script src="assets/vendor/chart.js/Chart.min.js"></script>-->
<script src="assets/vendor/datatables/jquery.dataTables.js"></script>
<script src="assets/vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="assets/js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="assets/js/demo/datatables-demo.js"></script>
<script src="assets/js/demo/chart-area-demo.js"></script>

<script>
    var detail = document.querySelector("#more");
    var visible = document.querySelector("#visible");
    var hidden_block = document.querySelector("#hidden_block");


    detail.addEventListener("click", function () {
        hidden_block.classList.toggle('visible_block');
    });


    visible.addEventListener("click", function () {
        alert("click");
    });


</script>


</body>
</html>



