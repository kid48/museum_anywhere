<?php


require_once "config.php";
require_once "db.php";

$install = "CREATE TABLE IF NOT EXISTS `check_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_locations` int(11) NOT NULL,
  `pos_x` int(11) NOT NULL,
  `pos_y` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `descript` text,
  `author` varchar(255) DEFAULT NULL,
  `image` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;";

mysqli_query($connection,$install);

if (mysqli_query($connection,$install)){
    echo "table check_points created<br>";
}

$install = "CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_objects` int(11) NOT NULL,
  `img` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `descript` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;";

if (mysqli_query($connection,$install)){
    echo "table locations created<br>";
}


$install = "CREATE TABLE IF NOT EXISTS `objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `descript` varchar(255) DEFAULT NULL,
  `poster` text,
  `video_poster` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
";

if (mysqli_query($connection,$install)){
    echo "table objects created<br>";
}

$install = "CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;";

if (mysqli_query($connection,$install)){
    echo "table users created<br>";
}

