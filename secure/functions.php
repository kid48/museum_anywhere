<?php
require_once "config.php";
require_once "db.php";


/**
 * @return array
 */
function get_all_museums()
{
    global $connection;
    $output = [];

    $query = "SELECT * FROM objects";
    $request = mysqli_query($connection, $query);

    while ($response = mysqli_fetch_assoc($request)) {
        $output[] = $response;
    }

    return $output;
}

/**
 * @return array|null
 */
function get_museum()
{
    global $connection;

    if (isset($_GET['id']) && !empty($_GET['id'])) {
        $id = $_GET['id'];
        $query = "SELECT * FROM objects WHERE id=" . $id;
        $request = mysqli_query($connection, $query);

        $response = mysqli_fetch_assoc($request);

        mysqli_free_result($request);

        return $response;


    }

}


function get_first_location()
{
    global $connection;

    if (isset($_GET['id']) && !empty($_GET['id'])) {
        $id = $_GET['id'];
        $query = "SELECT id FROM locations WHERE id_objects=" . $id . " LIMIT 1";
        $request = mysqli_query($connection, $query);

        $response = mysqli_fetch_assoc($request);

        mysqli_free_result($request);

        return $response;


    }
}

/**
 * @return array|bool
 */
function get_current_location()
{
    if (isset($_GET['id_objects']) && !empty($_GET['id_objects']) && isset($_GET['id']) && !empty($_GET['id'])) {
        global $connection;
        $current_location = [];

        $id_objects = trim($_GET['id_objects']);
        $id = trim($_GET['id']);
        $query = "SELECT * FROM locations WHERE id_objects=" . $id_objects . " AND id=" . $id;
        $request = mysqli_query($connection, $query);

        $response = mysqli_fetch_assoc($request);
        $current_location = $response;

        mysqli_free_result($request);

        # считаю количество всех данных локаций по музею
        $query = "SELECT COUNT(*) FROM locations WHERE id_objects=" . $id_objects;
        $request = mysqli_query($connection, $query);

        $response = mysqli_fetch_row($request);
        $current_location['count_location'] = $response[0];



        return $current_location;


    }

    return false;


}


/**
 * @return array
 */
function get_all_location()
{
    if (isset($_GET['id_objects']) && !empty($_GET['id_objects'])) {
        global $connection;
        $all_location = [];
        $id_objects = trim($_GET['id_objects']);
        $query = "SELECT * FROM locations WHERE id_objects=" . $id_objects;
        $request = mysqli_query($connection, $query);
        while ($response = mysqli_fetch_assoc($request)) {
            $all_location[] = $response;
        }

        return $all_location;
    }
}


/**
 * @return array|bool
 */
function get_items_for_current_location()
{
    if (isset($_GET['id']) && !empty($_GET['id'])) {
        global $connection;
        $output_items = [];

        $id = trim($_GET['id']);
        $query = "SELECT * FROM check_points WHERE id_locations=" . $id;
        $request = mysqli_query($connection, $query);

        while ($response = mysqli_fetch_assoc($request)) {
            $output_items[] = $response;
        }

        return $output_items;

    }

    return false;


}


function create_new_location()
{
    global $connection;
    if (isset($_POST['submit'])) {
        $id_objects = $_POST['id_objects'];
        $img = $_POST['img'];
        $title = $_POST['title'];
        $descript = $_POST['descript'];

        $query = "INSERT INTO locations (";
        $query .= "id_objects,img,title,descript";
        $query .= ") VALUES (";
        $query .= "'{$id_objects}', '{$img}', '{$title}', '{$descript}') ";


        $result = mysqli_query($connection, $query);
        if ($result) {

            header("Location: " . MAIN_URL . "/location_list.php?id_objects=" . $id_objects);

        } else {
            return false;
        }


    }
}

/**
 * @return string
 */
function create_new_museum()
{
    global $connection;
    if (isset($_POST['submit'])) {
        $name = $_POST['name'];
        $description = $_POST['description'];
        $poster = $_POST['poster'];
        $video_poster = $_POST['video_poster'];

        $query = "INSERT INTO objects (";
        $query .= "name,descript,poster,video_poster";
        $query .= ") VALUES (";
        $query .= "'{$name}', '{$description}', '{$poster}', '{$video_poster}') ";

        $result = mysqli_query($connection, $query);
        if ($result) {
            //return mysqli_insert_id($connection);
            header("Location: " . MAIN_URL . "/create_location.php?id=" . mysqli_insert_id($connection));
        } else {
            return false;
        }


    }
}

/**
 * @return bool
 */
function create_checkpoint()
{

//	if ( isset( $_GET['id_objects'] ) && isset( $_GET['id'] ) ) {
    global $connection;
    if (isset($_POST['submit'])) {
        $title = $_POST['title'];
        $img = $_POST['img'];
        $description = $_POST['description'];
        $id_locations = $_GET['id'];
        $id_objects = $_GET['id_objects'];
        $pos_y = $_POST['pos_y'];
        $pos_x = $_POST['pos_x'];
        $author = $_POST['author'];


        $query = "INSERT INTO check_points (";
        $query .= "id_locations,pos_x,pos_y,title,descript,author,image";
        $query .= ") VALUES (";
        $query .= "{$id_locations},{$pos_x}, {$pos_y}, '{$title}', '{$description}', '{$author}', '{$img}' )";

        $result = mysqli_query($connection, $query);
        if ($result) {
            $_SESSION['message'] = "SUCCESSFULL";
            header("Location:" . MAIN_URL . "/location_list.php?id_objects=" . $id_objects);

        } else {
            return false;
        }

    }

}

/**
 *
 */
function login()
{
    if (isset($_POST["login"])) {
        global $connection;


// Для проверки пароля в качестве salt следует передавать результат работы
// crypt() целиком во избежание проблем при использовании различных
// алгоритмов (как уже было отмечено выше, стандартный DES-алгоритм
// использует 2-символьную salt, а MD5 - 12-символьную.

//if (crypt($user_input, $password) == $password) {
//   echo "Пароль верен !";
//}

        if (!empty($_POST['username']) && !empty($_POST['password'])) {
            $username = htmlspecialchars($_POST['username']);
            $password = htmlspecialchars($_POST['password']);


            $query = "SELECT * FROM users WHERE username='" . $username . "' AND password='" . $password . "'";
            $query = mysqli_query($connection, $query);

            $numrows = mysqli_num_rows($query);
            if ($numrows != 0) {
                while ($row = mysqli_fetch_assoc($query)) {
                    $dbusername = $row['username'];
                    $dbpassword = $row['password'];
                }
                if ($username == $dbusername && $password == $dbpassword) {
                    // старое место расположения
                    //  session_start();
                    $_SESSION['session_username'] = $username;
                    /* Перенаправление браузера */
                    header("Location: index.php");
                }
            } else {
                //  $message = "Invalid username or password!";

                echo "Invalid username or password!";
            }
        } else {
            $message = "All fields are required!";
            echo $message;
        }
    }
}

function register()
{
    if (isset($_POST["register"])) {
        global $connection;
        $message = "";
        if (!empty($_POST['full_name']) && !empty($_POST['email']) && !empty($_POST['username']) && !empty($_POST['password'])) {
            $full_name = htmlspecialchars($_POST['full_name']);
            $email = htmlspecialchars($_POST['email']);
            $username = htmlspecialchars($_POST['username']);
            $password = htmlspecialchars($_POST['password']);
            $query = mysqli_query($connection, "SELECT * FROM users WHERE username='" . $username . "'");
            $numrows = mysqli_num_rows($query);
            if ($numrows == 0) {
                $sql = "INSERT INTO users
  (full_name, email, username,password)
	VALUES('$full_name','$email', '$username', '$password')";
                $result = mysqli_query($connection, $sql);
                if ($result) {
                    header("Location: login.php");
                } else {
                    $message = "Failed to insert data information!";
                }
            } else {
                $message = "That username already exists! Please try another one!";
            }
        } else {
            $message = "All fields are required!";

        }
    }
}


function get_all_user()
{
    global $connection;
    $output = [];

    $query = "SELECT * FROM users";
    $request = mysqli_query($connection, $query);

    while ($response = mysqli_fetch_assoc($request)) {
        $output[] = $response;
    }

    return $output;


}

function edit_museum() {

    global $connection;


}


?>

