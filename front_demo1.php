<?php
//require_once "secure/config.php";
//require_once "secure/functions.php";
//$location = get_current_location();



?>
<!doctype html>
<html>
<head>
    <style>

        #hidden_block {
            display: none;
        }

        .visible_block {
            display: block !important;
        }

        h1 {
            margin: 0 !important;
            padding: 0;
            font-size: 27px !important;
        }

        header {
            width: auto;
            overflow: hidden;
        }

        #visible, #title, #more {
            float: left;
        }

        #visible img, #more img {
            width: 30%;
            display: block;
            margin: 0 auto;
        }

        #visible, #more {
            background: #d6e7d7;
            border: 3px solid #0b2e13;
            border-left-width: 0;
            padding: 2px 0;
        }

        #title {
            background-color: #D7D8DF;
            width: 900px;
            border: solid 3px #0b2e13;
            padding: 8px 0;
        }

        #more {
            width: 150px;
        }

        #more:hover {
            cursor: pointer;
            background: #8da98b;
        }

        #visible {
            width: 150px;
        }

        #visible:hover {
            cursor: pointer;
            background: #8da98b;
        }

        .main {
            width: 1200px;
            margin: 0 auto;
            height: 90vh;
        }

        #wrapper {
            display: block;
            margin: 0 auto;
            width: 100%;
            overflow: hidden;
        }

        .side {
            background: #ada8d8;
            height: 100vh;
            width: 6%;
        }

        .side:hover {
            background: #8E9A92;
        }

        .side, .main {
            float: left;
        }
        #hidden_more1 {
            border: solid #000000 3px;
        }

        /*=====================================================*/

        #pointer_div {
            background: url('http://www.netherlands-tourism.com/wp-content/uploads/2015/01/Frans-Hals-Museum-Haarlem-manieristenzaal.jpg') center 20%/100%;
            width: 1250px;
            height: 90vh;
            margin: 0 auto;
        }

        #cross, .cross {
            position: relative;
            visibility: hidden;
            z-index: 2;
            width: 20px;
            margin: 0 0 0 -14px;
        }

        /*=====================================================*/


    </style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="assets/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="assets/css/sb-admin.css" rel="stylesheet">

</head>
<body>


<div id="wrapper">
    <div class="side"></div>

    <div class="main">


        <header>

            <div id="title">
               <h1><?php //echo $location[0]['title']; ?> Перша зала</h1>
            </div>

            <div id="more">
                <img src="img/expand-button.svg" alt="">
            </div>

            <div id="visible">
                <img src="img/eye.svg" alt="">
            </div>

            <section id="hidden_block">
                <p>
                   <?php //echo $location[0]['descript']?> Це опис перщої зали
                </p>
            </section>

        </header>

        <form name="pointform" method="post">
            <div id="pointer_div" onclick="point_it(event)">
                <!--                <img src="point.png" id="cross" style="">-->
            </div>
        </form>

    </div>

    <div class="side"></div>
</div>
<div id="hidden_more" class="alert alert-light" role="alert">
    <p>

    </p>
</div>

<script>
    var counter = 0;

    // function point_it(event) {
    //     var clickPoint = document.createElement("img");
    //     var pointer_div = document.getElementById("pointer_div");
    //     var all_points = document.querySelectorAll('.cross');
    //
    //     if (counter != 0) {
    //         all_points[all_points.length - 1].outerHTML = "";
    //     } else {
    //         counter++;
    //     }
    //
    //
    //     pos_x = event.offsetX ? (event.offsetX) : event.pageX - document.getElementById("pointer_div").offsetLeft;
    //     pos_y = event.offsetY ? (event.offsetY) : event.pageY - document.getElementById("pointer_div").offsetTop;
    //
    //     clickPoint.classList.add("cross");
    //     clickPoint.setAttribute("src", "point.png");
    //     clickPoint.style.visibility = "visible";
    //
    //     clickPoint.style.left = (pos_x ) + "px";
    //     clickPoint.style.top = pos_y + "px";
    //
    //     pointer_div.appendChild(clickPoint);
    // }


    document.body.onload = function () {

        addElement();

        var all_points = document.querySelectorAll(".cross");

        var hidden_more = document.createElement('div');
        hidden_more.className = "alert alert-success";


        console.log(all_points);

        for (var a = 0; a < all_points.length; a++) {
            var KocTuJIb = [
                "Це картина 1",
                "Це картина 2",
                "Це картина 3",
                "Це картина 4",
                "Це картина 5"
            ];

            all_points[a].addEventListener('click', function () {
                if (this.classList[1] == "cross_0") {
                   document.getElementById("hidden_more").innerText = (KocTuJIb[0]);
                } else if (this.classList[1] == "cross_1") {
                    document.getElementById("hidden_more").innerText = (KocTuJIb[1]) ;
                } else if (this.classList[1] == "cross_2") {
                    document.getElementById("hidden_more").innerText =(KocTuJIb[2]);
                } else if (this.classList[1] == "cross_3") {
                    document.getElementById("hidden_more").innerText =(KocTuJIb[3]);
                } else if (this.classList[1] == "cross_4") {
                    document.getElementById("hidden_more").innerText =(KocTuJIb[4]);
                }

            });
        }
    };

    var check_points = [
        [503, 246],
        [625, 247],
        [738, 247],
        [129, 140],
        [904, 227],
//        [688 - 32, 331],
//        [326 - 48, 464],


        // [326 - 64, 464],
        // [420 - 80, 277],
        // [420 - 96, 277],
        // [420 - 112, 277],
        // [420 - 128, 277],
    ];


    function addElement() {


        // create a new point elements
        for (var i = 0; i < check_points.length; i++) {
            var newPoint = document.createElement("img");
            newPoint.classList.add("cross");
            newPoint.classList.add("cross_" + i);
            newPoint.setAttribute("src", "point.png");
            newPoint.style.visibility = "visible";

            newPoint.style.left = check_points[i][0] + "px";
            newPoint.style.top = check_points[i][1] + "px";

            // and give it some content
            var newContent = document.createTextNode("Hi there and greetings!");

            // add the newly created element and its content into the DOM
            var pointer_div = document.getElementById("pointer_div");

            // add the text node to the newly created div
            pointer_div.appendChild(newPoint);


        }
    }


</script>


<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<!--<script src="assets/vendor/chart.js/Chart.min.js"></script>-->
<script src="assets/vendor/datatables/jquery.dataTables.js"></script>
<script src="assets/vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="assets/js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="assets/js/demo/datatables-demo.js"></script>
<script src="assets/js/demo/chart-area-demo.js"></script>

<script>
    var detail = document.querySelector("#more");
    var visible = document.querySelector("#visible");
    var hidden_block = document.querySelector("#hidden_block");


    detail.addEventListener("click", function () {
        hidden_block.classList.toggle('visible_block');
    });


    visible.addEventListener("click", function () {
        alert("click");
    });


</script>


</body>
</html>
