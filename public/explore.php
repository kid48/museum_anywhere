<?php
require_once "../secure/config.php";
require_once "../secure/functions.php";
$location = get_current_location();


$items = get_items_for_current_location();


?>
<!doctype html>
<html>
<head>
    <style>

        #hidden_block {
            display: none;
        }

        .visible_block {
            display: block !important;
        }

        h1 {
            margin: 0 !important;
            padding: 0;
            font-size: 27px !important;
        }

        header {
            width: auto;
            overflow: hidden;
        }

        #visible, #title, #more {
            float: left;
        }

        #visible img, #more img {
            width: 15%;
            display: block;
            margin: 0 auto;
        }

        #visible, #more {
            background: #d6e7d7;
            border: 3px solid #0b2e13;
            border-left-width: 0;
            padding: 2px 0;
        }

        #title {
            background-color: #D7D8DF;
            width: 900px;
            border: solid 3px #0b2e13;
            padding: 8px 0;
        }

        #more {
            width: 300px;
        }

        #more:hover {
            cursor: pointer;
            background: #8da98b;
        }

        #visible {
            width: 150px;
        }

        #visible:hover {
            cursor: pointer;
            background: #8da98b;
        }

        .main {
            width: 1200px;
            margin: 0 auto;
            height: 90vh;
        }

        #wrapper {
            display: block;
            margin: 0 auto;
            width: 100%;
            overflow: hidden;
        }

        .side {
            background: #ada8d8;
            height: 100vh;
            width: 6%;
        }

        .side:hover {
            background: #8E9A92;
        }

        .side, .main {
            float: left;
        }

        #hidden_more1 {
            border: solid #000000 3px;
        }

        /*=====================================================*/

        #pointer_div {
            background: url('<?php echo $location['img'];  ?>') center 20%/100%;
            width: 1250px;
            height: 90vh;
            margin: 0 auto;
        }

        #cross, .cross {
            position: relative;
            visibility: hidden;
            z-index: 2;
            width: 20px;
            margin: 0 0 0 -14px;
        }

        /*=====================================================*/


    </style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="../assets/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../assets/css/sb-admin.css" rel="stylesheet">

</head>
<body>


<div id="wrapper">

    <?php if ($location['position'] != 1): ?>

        <a class="side"
           href="http://localhost/museum_anywhere/public/explore.php?id=<?php echo $location['id'] - 1; ?>&id_objects=<?php echo $_GET['id_objects']; ?>"
        >&#8656;</a>

    <?php else: ?>

        <a class="side"
        >&#8656;</a>

    <?php endif; ?>

    <div class="main">


        <header>

            <div id="title">
                <h1><?php echo $location['title']; ?> </h1>
            </div>

            <div id="more">
                <img src="../img/expand-button.svg" alt="">
            </div>

            <!--            <div id="visible">-->
            <!--                <img src="../img/eye.svg" alt="">-->
            <!--            </div>-->

            <section id="hidden_block">
                <p>
                    <?php echo $location['descript'] ?>
                </p>
            </section>

        </header>

        <form name="pointform" method="post">
            <div id="pointer_div"></div>
        </form>

    </div>


    <?php if ($location['position'] != $location['count_location']): ?>

        <a class="side"
           href="http://localhost/museum_anywhere/public/explore.php?id=<?php echo $location['id'] + 1; ?>&id_objects=<?php echo $_GET['id_objects']; ?>"
        >&#8658;</a>

    <?php else: ?>

        <a class="side"
        >&#8658;</a>

    <?php endif; ?>

</div>


<script>

    document.body.onload = function () {

        addElement();

        var all_points = document.querySelectorAll(".cross");

        var hidden_more = document.createElement('div');
        hidden_more.className = "alert alert-success";


        var KocTuJIb = [
            <?php foreach ($items as $item): ?>
            {
                id: "<?php echo $item['id'];?>",
                description: "<?php echo $item['descript'];?>",
                author: "<?php echo $item['author']; ?>",
                title: "<?php echo $item['title']; ?>",
                id_location: "<?php echo $item['id_locations']; ?>",
                image: "<?php  echo $item['image']; ?>"
            },
            <?php endforeach; ?>
        ];

        console.log(all_points);


        for (var a = 0; a < all_points.length; a++) {

            all_points[a].addEventListener('click', function () {

                var current_point = this;

                for (var counter = 0; counter < KocTuJIb.length; counter++) {
                    if (current_point.getAttribute('data-point-id') == KocTuJIb[counter].id) {
                        var mdc = document.querySelector('#modal_dynamic_content');
                        var output = "";

                        console.log(KocTuJIb[counter]);

                        output = "<div class='row justify-content-center'><div class='col-lg-6'>";
                        output += "<img class='w-100' src='../" + KocTuJIb[counter].image + "'></div>";

                        output += "<div class='col-lg-6'>";

                        output += "<h2>" + KocTuJIb[counter].title + "</h2>";
                        output += "<p>" + KocTuJIb[counter].description + "</p>";
                        // output += "<br>id_location: " + KocTuJIb[counter].id_location;

                        output += "</div>";


                        output += "</div> <!-- end row--->";

                        mdc.innerHTML = output;
                    }
                }

            });
        }
    };

    var check_points = [
        <?php foreach ($items as $item): ?>
        [<?php echo $item['pos_x'];?>,<?php echo $item['pos_y'];?>, <?php echo $item['id'];?> ],
        <?php endforeach; ?>
    ];


    function addElement() {


        // create a new point elements
        for (var i = 0; i < check_points.length; i++) {
            var newPoint = document.createElement("img");
            newPoint.classList.add("cross");
            newPoint.classList.add("cross_" + i);
            newPoint.setAttribute("src", "../point.png");
            newPoint.setAttribute("data-point-id", check_points[i][2]);


            newPoint.setAttribute("data-toggle", "modal");
            newPoint.setAttribute("data-target", "#detailModalCenter");


            newPoint.style.visibility = "visible";

            newPoint.style.left = check_points[i][0] + "px";
            newPoint.style.top = check_points[i][1] + "px";

            // and give it some content
            var newContent = document.createTextNode("Hi there and greetings!");

            // add the newly created element and its content into the DOM
            var pointer_div = document.getElementById("pointer_div");

            // add the text node to the newly created div
            pointer_div.appendChild(newPoint);


        }
    }


</script>


<script src="../assets/vendor/jquery/jquery.min.js"></script>
<script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="../assets/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<!--<script src="assets/vendor/chart.js/Chart.min.js"></script>-->
<script src="../assets/vendor/datatables/jquery.dataTables.js"></script>
<script src="../assets/vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="../assets/js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="../assets/js/demo/datatables-demo.js"></script>
<script src="../assets/js/demo/chart-area-demo.js"></script>

<script>
    var detail = document.querySelector("#more");
    var visible = document.querySelector("#visible");
    var hidden_block = document.querySelector("#hidden_block");


    detail.addEventListener("click", function () {
        hidden_block.classList.toggle('visible_block');
    });


    visible.addEventListener("click", function () {
        alert("click");
    });


</script>


<!-- Modal -->
<div class="modal fade" id="detailModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-body" id="modal_dynamic_content"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
