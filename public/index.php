<?php

require_once "../secure/config.php";
require_once "../secure/functions.php";

require_once "theme_parts/header.php";

$museums = get_all_museums();



?>


<div class="container-fluid p-o">
    <div class="row">
        <h1 class="text-center display-4 col-lg-12">Museum anywhere </h1>
        <p class="lead text-center small col-lg-12">
            Музей де завгодно та коли завгодно
        </p>
    </div>
    <div class="row">

        <?php  foreach ($museums as $museum):?>

        <div class="col-lg-3 museum-item" style="background: url('<?php echo $museum['poster']; ?>') center 20%/131% no-repeat; height: 250px; ">
            <a href="museum_detail.php?id=<?php echo $museum['id']; ?>" >
                <?php echo $museum['name']; ?>
            </a>
        </div>
        <?php endforeach; ?>

    </div>

</div>

<?php require_once "theme_parts/footer.php"; ?>

