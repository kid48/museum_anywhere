<?php
require_once "../secure/config.php";
require_once "../secure/db.php";
require_once "../secure/functions.php";

if ( isset( $_GET['id'] ) ) {

    $current_museum = get_museum();
    $first_location = get_first_location();


    $first_location_id = $first_location['id'];

} else {
    //404
}
require_once "theme_parts/header.php";

?>

    <style>
        #main_museum_block {
            background: url("<?php echo $current_museum['poster']; ?>") center 20%/100%;
            height: 100vh;
            overflow: hidden;
        }

        #submain_mu_block {
            background: rgba(238, 238, 238, 0.47);
            height: 100vh;
            overflow: hidden;
        }

        #submain_mu_block p {
            font-size: 23px;
            text-align: center;
        }

        #submain_mu_block a {
            margin: auto;
            display: block;

        }

        #wrapper {
            position: absolute;
            top: 240px;
            left: 0;
            width: 100%;
        }

        #wrapper .btn {
            width: 270px;
            border-radius: 11px !important;
            background: rgba(255, 255, 255, 0.81);
        }

    </style>

    <div class="container-fluid p-0" id="main_museum_block">
        <div class="container-fluid p-0 " id="submain_mu_block">

            <div id="wrapper">
                <h1 class="text-center">
                    <?php echo $current_museum['name']; ?>
                </h1>

                <p>
                    <?php echo $current_museum['descript']; ?>
                </p>
                <a type="button" class="btn  btn-lg"
                   href="explore.php?id=<?php echo $first_location_id; ?>&id_objects=<?php echo $current_museum['id']; ?>&id_museum"><i>Lets
                        GO!</i></a>
            </div>
        </div>
    </div>


<?php require_once "theme_parts/footer.php"; ?>