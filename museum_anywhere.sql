-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 10 2018 г., 12:36
-- Версия сервера: 5.7.21
-- Версия PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `museum_anywhere`
--

-- --------------------------------------------------------

--
-- Структура таблицы `check_points`
--

DROP TABLE IF EXISTS `check_points`;
CREATE TABLE IF NOT EXISTS `check_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_museum` int(11) NOT NULL,
  `id_locations` int(11) NOT NULL,
  `pos_x` int(11) NOT NULL,
  `pos_y` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `descript` text,
  `author` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `check_points`
--

INSERT INTO `check_points` (`id`, `id_museum`, `id_locations`, `pos_x`, `pos_y`, `title`, `descript`, `author`) VALUES
(1, 0, 1, 1, 0, '111', NULL, NULL),
(2, 0, 1, 1, 0, '222', NULL, NULL),
(3, 0, 1, 1, 0, '333', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_objects` int(11) NOT NULL,
  `img` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `descript` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `locations`
--

INSERT INTO `locations` (`id`, `id_objects`, `img`, `title`, `descript`) VALUES
(1, 1, 'https://static.tonkosti.ru/images/2/26/%D0%9C%D1%83%D0%B7%D0%B5%D0%B9_%D0%B8%D1%81%D1%82%D0%BE%D1%80%D0%B8%D0%B8_%D0%B8%D1%81%D0%BA%D1%83%D1%81%D1%81%D1%82%D0%B2_%D0%B2_%D0%92%D0%B5%D0%BD%D0%B5%2C_%D0%BE%D0%B4%D0%B8%D0%BD_%D0%B8%D0%B7_%D0%B7%D0%B0%D0%BB%D0%BE%D0%B2.jpg', 'первая зала', 'Первая зала бла бла бла'),
(2, 1, 'https://ru.moscovery.com/wp-content/uploads/2016/03/%D0%9C%D0%BD%D0%BE%D0%B3%D0%BE%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D0%B8%D0%B5-%D1%8D%D0%BA%D1%81%D0%BF%D0%BE%D0%BD%D0%B0%D1%82%D0%BE%D0%B2.jpg', 'вторая зала', 'вторая зала бла бла бла');

-- --------------------------------------------------------

--
-- Структура таблицы `objects`
--

DROP TABLE IF EXISTS `objects`;
CREATE TABLE IF NOT EXISTS `objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `descript` varchar(255) DEFAULT NULL,
  `poster` text,
  `video_poster` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `objects`
--

INSERT INTO `objects` (`id`, `name`, `descript`, `poster`, `video_poster`) VALUES
(1, 'School Museum', 'School Museum descript', 'http://www.playford.sa.gov.au/webdata/resources/images/Uleybury%20School.jpg', NULL),
(3, 'Luvr', 'It is Luvr', 'https://www.metaphor.eu/wp-content/uploads/winchester-school-museum-1.jpg', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
