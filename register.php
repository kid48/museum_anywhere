<?php
require_once "secure/config.php";
require_once "secure/db.php";
require_once "secure/functions.php";
if ( isset( $_POST['register'] ) && $_POST['password'] == $_POST['confirm_password'] ) {
	register();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Register</title>

    <!-- Bootstrap core CSS-->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="assets/css/sb-admin.css" rel="stylesheet">

</head>

<body class="bg-dark">

<div class="container">
    <div class="card card-register mx-auto mt-5">
        <div class="card-header">Зареєструвати користувача</div>
        <div class="card-body">
            <form method="post" action="register.php">
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input type="text" name="full_name" id="firstName" class="form-control"
                                       placeholder="Full name"
                                       required="required" autofocus="autofocus">
                                <label for="firstName">ПІБ</label>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input type="text" name="username" id="username" class="form-control"
                                       placeholder="username"
                                       required="required" autofocus="autofocus">
                                <label for="username">Нікнейм</label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input name="email" type="email" id="inputEmail" class="form-control"
                               placeholder="Email address"
                               required="required">
                        <label for="inputEmail">Електронна адреса</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input name="password" type="password" id="inputPassword" class="form-control"
                                       placeholder="Password"
                                       required="required">
                                <label for="inputPassword">Пароль</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input name="confirm_password" type="password" id="confirmPassword" class="form-control"
                                       placeholder="Confirm password" required="required">
                                <label for="confirmPassword">Пароль ще раз</label>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary btn-block" name="register" value="Зареєструвати">
            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="login.php">Війти</a>

            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
