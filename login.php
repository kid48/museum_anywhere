<?php
require_once "secure/config.php";
require_once "secure/functions.php";
require_once "secure/db.php";
session_start();
if ( isset( $_SESSION["session_username"] ) ) {
	// вывод "Session is set"; // в целях проверки
	header( "Location: index.php" );
}

if ( isset( $_POST['login'] ) ) {
	login();
}


?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Війти</title>

    <!-- Bootstrap core CSS-->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="assets/css/sb-admin.css" rel="stylesheet">

</head>

<body class="bg-dark">

<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Вхід</div>
        <div class="card-body">
            <form action="login.php" method="post">
                <div class="form-group">
                    <div class="form-label-group">
                        <input name="username" type="text" id="username" class="form-control" placeholder="Нікнейм"
                               required="required" autofocus="autofocus">
                        <label for="username">Нікнейм</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input name="password" type="password" id="inputPassword" class="form-control"
                               placeholder="Пароль"
                               required="required">
                        <label for="inputPassword">Пароль</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="remember-me">
                            Запам'ятать мене
                        </label>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary btn-block" name="login" value="Login">

            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="register.php">Зареєструвати</a>
                <a class="d-block small" href="forgot-password.html">Забув Пароль?</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
