<?php

session_start();
require_once "secure/config.php";
require_once "secure/db.php";
require_once "secure/functions.php";


$locations = get_all_location();

?>

<?php

if (!isset($_SESSION["session_username"])):
    header("location:login.php");
else:
    ?>
    <?php require_once "pages/header.php"; ?>


    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Admin - Dashboard</title>

        <!-- Bootstrap core CSS-->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <!-- Page level plugin CSS-->
        <link href="assets/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="assets/css/sb-admin.css" rel="stylesheet">

    </head>

    <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

        <a class="navbar-brand mr-1" href="index.php">Museum_anywhere</a>

        <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>

        <!-- Navbar Search -->
        <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for..." aria-label="Search"
                       aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="button">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>

        <!-- Navbar -->
        <ul class="navbar-nav ml-auto ml-md-0">
            <!--            <li class="nav-item dropdown no-arrow mx-1">-->
            <!--                <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"-->
            <!--                   aria-haspopup="true" aria-expanded="false">-->
            <!--                    <i class="fas fa-bell fa-fw"></i>-->
            <!--                    <span class="badge badge-danger">9+</span>-->
            <!--                </a>-->
            <!--                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">-->
            <!--                    <a class="dropdown-item" href="#">Action</a>-->
            <!--                    <a class="dropdown-item" href="#">Another action</a>-->
            <!--                    <div class="dropdown-divider"></div>-->
            <!--                    <a class="dropdown-item" href="#">Something else here</a>-->
            <!--                </div>-->
            <!--            </li>-->
            <!--            <li class="nav-item dropdown no-arrow mx-1">-->
            <!--                <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown"-->
            <!--                   aria-haspopup="true" aria-expanded="false">-->
            <!--                    <i class="fas fa-envelope fa-fw"></i>-->
            <!--                    <span class="badge badge-danger">7</span>-->
            <!--                </a>-->
            <!--                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">-->
            <!--                    <a class="dropdown-item" href="#">Action</a>-->
            <!--                    <a class="dropdown-item" href="#">Another action</a>-->
            <!--                    <div class="dropdown-divider"></div>-->
            <!--                    <a class="dropdown-item" href="#">Something else here</a>-->
            <!--                </div>-->
            <!--            </li>-->
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-user-circle fa-fw"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">Settings</a>
                    <a class="dropdown-item" href="#">Activity Log</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="logout.php" data-toggle="modal" data-target="#logoutModal">Logout</a>
                </div>
            </li>
        </ul>

    </nav>

    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="sidebar navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">
                    <i class="fas fa-hotel"></i>&nbsp;
                    <span>Музеї</span>
                </a>
            </li>
            <!--            <li class="nav-item dropdown">-->
            <!--                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown"-->
            <!--                   aria-haspopup="true" aria-expanded="false">-->
            <!--                    <i class="fas fa-fw fa-folder"></i>-->
            <!--                    <span>Страницы</span>-->
            <!--                </a>-->
            <!--                <div class="dropdown-menu" aria-labelledby="pagesDropdown">-->
            <!--                    <h6 class="dropdown-header">Login Screens:</h6>-->
            <!--                    <a class="dropdown-item" href="login.php">Login</a>-->
            <!--                    <a class="dropdown-item" href="register.php">Register</a>-->
            <!--                    <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>-->
            <!--                    <div class="dropdown-divider"></div>-->
            <!--                    <h6 class="dropdown-header">Other Pages:</h6>-->
            <!--                    <a class="dropdown-item" href="404.html">404 Page</a>-->
            <!--                    <a class="dropdown-item" href="blank.html">Blank Page</a>-->
            <!--                </div>-->
            <!--            </li>-->
            <!-- <li class="nav-item">
              <a class="nav-link" href="charts.html">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>Charts</span></a>
            </li> -->

            <li class="nav-item">
                <a class="nav-link" href="users.php">
                    <i class="fas fa-users"></i>&nbsp;
                    <span>Користувачі</span></a>
            </li>
        </ul>

        <div id="content-wrapper">


            <div class="container-fluid">


                <?php if (isset($_SESSION['message'])): ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong><?php echo $_SESSION['message']; unset($_SESSION['message']); ?></strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php endif; ?>


                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Музеї</a>
                    </li>
                    <li class="breadcrumb-item active">Всі локації поточного музею</li>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="create_location.php?id=<?php echo $_GET['id_objects'] ?>" class="btn btn-info btn-md">
                        Створити Локацію
                    </a>
                </ol>

                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>

                    <th scope='col'>
                        Зображення
                    </th>
                    <th scope='col'>
                        Назва
                    </th>
                    <th scope='col'>
                        Опис
                    </th>
                    <th scope="col">
                        Позиція
                    </th>
                    <th scope="col">
                        Створити точку для локаціїї
                    </th>
                    </thead>
                    <tbody>

                    <?php if (!empty($locations)): ?>
                        <?php foreach ($locations as $location): ?>

                            <tr>
                                <td>
                                    <img src="<?php echo $location['img']; ?>" alt="" width="35%">
                                </td>
                                <td>
                                    <?php echo $location['title']; ?>
                                </td>
                                <td>
                                    <?php echo $location['descript']; ?>
                                </td>

                                <td>
                                    <?php echo $location['position']; ?>
                                </td>

                                <td>
                                    <a class="btn btn-primary"
                                       href="<?php echo MAIN_URL . "/create_checkpoint.php?id=" . $location['id'] . "&id_objects=" . $_GET['id_objects']; ?>">
                                        Створити точку для локаціїї
                                    </a>
                                </td>

                            </tr>

                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>


                <!--  <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div> -->


            </div>
            <!-- /.container-fluid -->

            <!-- Sticky Footer -->
            <!--            <footer class="sticky-footer">-->
            <!--                <div class="container my-auto">-->
            <!--                    <div class="copyright text-center my-auto">-->
            <!--                        <span>Copyright © Your Website 2018</span>-->
            <!--                    </div>-->
            <!--                </div>-->
            <!--            </footer>-->

        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Готовий покинути?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Виберіть "Вихід" нижче, якщо ви готові завершити поточний сеанс.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Скасувати</button>
                    <a class="btn btn-primary" href="logout.php">Вийти</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="assets/vendor/chart.js/Chart.min.js"></script>
    <script src="assets/vendor/datatables/jquery.dataTables.js"></script>
    <script src="assets/vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="assets/js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="assets/js/demo/datatables-demo.js"></script>
    <script src="assets/js/demo/chart-area-demo.js"></script>

    </body>

    </html>
<?php endif; ?>