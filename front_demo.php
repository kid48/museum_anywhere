<?php
require_once "secure/config.php";
require_once "secure/functions.php";

$i = 1;
//var_dump( $_POST );
//var_dump( get_items_for_current_location() );
$cord = get_items_for_current_location();

?>
<html>
<head>

    <style>
        #pointer_div {
            background-image: url('museum.jpg');
            width: 1250px;
            height: 90vh;
            margin: 0 auto;
        }

        #cross, .cross {
            position: relative;
            visibility: hidden;
            z-index: 2;
            width: 30px;
            margin: 0 0 0 -14px;
        }

        .form_pointer {

        }
    </style>


</head>
<body>

<div class="form_pointer" id="pointer_div" onclick="point_it(event)">
    <img src="point.png" id="cross" style="">
</div>

<script>


    document.body.onload = addElement;

    var check_points = [


		<?php foreach ($cord as $item): ?>
        [<?php echo $item['pos_x'] - $i * 16; ?>,
			<?php echo $item['pos_y']; ?>],
		<?php $i ++;?>
		<?php endforeach; ?>


        // [420, 277],
        // [688 - 32, 331],
        // [421 - 32, 166],
        // [326 - 48, 464],
        // [326 - 64, 464],
        // [420 - 80, 277],
        // [420 - 96, 277],
        // [420 - 112, 277],
        // [420 - 128, 277],
    ];


    function addElement() {
        // create a new point elements
        for (var i = 0; i < check_points.length; i++) {
            var newPoint = document.createElement("img");
            newPoint.classList.add("cross");
            newPoint.setAttribute("src", "point.png");
            newPoint.style.visibility = "visible";

            newPoint.style.left = check_points[i][0] + "px";
            newPoint.style.top = check_points[i][1] + "px";

            // and give it some content
            var newContent = document.createTextNode("Hi there and greetings!");

            // add the newly created element and its content into the DOM
            var pointer_div = document.getElementById("pointer_div");

            // add the text node to the newly created div
            pointer_div.appendChild(newPoint);
        }
    }

</script>
</body>
